export default class Carousel {
  constructor(element) {
    this.wheel = this.wheel.bind(this);
    this.mousedown = this.mousedown.bind(this);
    this.mousemove = this.mousemove.bind(this);
    this.mouseup = this.mouseup.bind(this);
    this.touchstart = this.touchstart.bind(this);
    this.touchmove = this.touchmove.bind(this);
    this.touchend = this.touchend.bind(this);
    this.update = this.update.bind(this);
    this.resize = this.resize.bind(this);
    this.element = element;
    this.slides = document.createElement('div');
    this.slides.className = 'carousel-slides';
    this.position = 0;
    this.velocity = 0;
    this.dragging = false;
    this.last = 0;

    this.deltas = [];
    this.touches = [];

    this.width = this.element.getBoundingClientRect().width;
    this.slideArray = [...this.element.children];
    this.slides.style.width = `${this.slideArray.length * this.width}px`;
    this.slideArray.forEach((slide, index) => {
      slide.style.width = `${this.width}px`;
      slide.style.left = `${index * this.width}px`;
      this.slides.appendChild(slide);
    });
    this.element.appendChild(this.slides);

    this.element.addEventListener('mousedown', this.mousedown);
    this.element.addEventListener('touchstart', this.touchstart);
    this.element.addEventListener('wheel', this.wheel);

    window.addEventListener('resize', this.resize);
    this.resize();
    this.update();
  }

  wheel(e) {
    this.deltas.push(e.deltaX);
    if (this.deltas.length === 5) {
      this.deltas.shift();
    }
    if (!this.docking && (Math.abs(this.deltas[3]) < Math.abs(this.deltas[2]) && Math.abs(this.deltas[2]) < Math.abs(this.deltas[1]) && Math.abs(this.deltas[1]) < Math.abs(this.deltas[0]))) {
      console.log('adjust the delta to help dock the carousel properly');
      this.docking = true;
      setTimeout(() => { this.docking = false; }, 1000);
    }
    e.preventDefault();
    this.velocity = -e.deltaX;
    this.position += this.velocity;
    this.slides.style.transform = `translateX(${this.position}px)`;
  }

  mousedown({ offsetX }) {
    this.start = offsetX;
    this.dragging = true;
    window.addEventListener('mousemove', this.mousemove);
    window.addEventListener('mouseup', this.mouseup);
    window.addEventListener('mouseout', this.mouseup);
  }

  mousemove({ offsetX }) {
    this.velocity = offsetX - this.start;
    this.position += this.velocity;
    this.slides.style.transform = `translateX(${this.position}px)`;
  }

  mouseup() {
    this.dragging = false;
    window.removeEventListener('mousemove', this.mousemove);
    let drift = 0;
    let vel = this.velocity;
    while (Math.abs(vel) >= 0.01) {
      vel *= 0.9;
      drift += vel;
    }
    this.projectedPosition = this.snapToWidth(this.position + drift);
    console.log(this.projectedPosition);
  }

  snapToWidth(amount) {
    return Math.round(amount / this.width) * this.width;
  }

  touchstart({ changedTouches }) {
    if (changedTouches.length > 1) return;
    this.touching = true;
    this.start = changedTouches[0].clientX;
    this.dragging = true;
    window.addEventListener('touchmove', this.touchmove);
    window.addEventListener('touchend', this.touchend);
  }

  touchmove(e) {
    this.dragging = true;
    this.velocity = e.changedTouches[0].clientX - this.start;
    this.position = this.velocity + this.last;
    this.slides.style.transform = `translateX(${this.position}px)`;
    this.touches.push(e.changedTouches[0].clientX);
    if (this.touches.length === 3) this.touches.shift();
  }

  touchend() {
    this.velocity = this.touches[1] - this.touches[0];
    this.touching = false;
    this.dragging = false;
    this.last = this.position;
    window.removeEventListener('touchmove', this.touchmove);
  }

  // TODO eventually only run when we know carousel is moving, otherwise go quiet
  update() {
    // drifting
    if (!this.dragging && Math.abs(this.velocity) > 0.01) {
      this.velocity *= 0.9;
      this.position += this.velocity;
      this.last = this.position;
      this.slides.style.transform = `translateX(${this.position}px)`;
    }

    // wrapping
    const { length } = this.slideArray;
    this.slideArray.forEach((slide) => {
      const left = parseInt(slide.style.left, 10);
      const position = this.position + left;

      if (this.velocity < 0 && position < -this.width) {
        slide.style.left = `${left + (this.width * length)}px`;
      }
      if (this.velocity > 0 && position > this.width) {
        slide.style.left = `${left - (this.width * length)}px`;
      }
    });

    // docking

    requestAnimationFrame(this.update);
  }

  start() {

  }

  resize() {
  }
}
