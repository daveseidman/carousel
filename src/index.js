import inobounce from 'inobounce';
import Carousel from './modules/Carousel';
import './index.scss';

const carousel = new Carousel(document.getElementsByClassName('carousel')[0]);
carousel.start();
